package com.galid.card_refund.domains.loss.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LossRepository extends JpaRepository<LossEntity, Long> {
}
